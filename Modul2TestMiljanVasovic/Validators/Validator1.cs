﻿using FluentValidation;
using Modul2TestMiljanVasovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.Validators
{
    public class Validator1 : AbstractValidator<KategorijaOdeceModel>
    {
        public Validator1()
        {

            RuleFor(p => p.KategorijaId)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().WithMessage("Niste uneli obavezna polja");

            RuleFor(p => p.NazivKategorije)
               .Cascade(CascadeMode.StopOnFirstFailure)
               .NotEmpty().WithMessage("Polje ne moze biti prazno")
               .Length(1, 60).WithMessage("Duzina mora biti izmedju 1 i 60 karaktera")
               .Must(BeAValidName).WithMessage("Morate uneti Naziv artikla koji je sacinjen od slova.");
        }

        protected bool BeAValidName(string NazivKategorije)
        {
            NazivKategorije = NazivKategorije.Replace(" ", "");
            NazivKategorije = NazivKategorije.Replace("-", "");
            return NazivKategorije.All(Char.IsLetterOrDigit);
        }
    }
}