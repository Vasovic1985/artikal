﻿using FluentValidation;
using Modul2TestMiljanVasovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.Validators
{
    public class Validator2 : AbstractValidator<TipOdeceModel>
    {
        public Validator2()
        {

            RuleFor(p => p.TipId)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().WithMessage("Niste uneli obavezna polja");

            RuleFor(p => p.NazivTipa)
               .Cascade(CascadeMode.StopOnFirstFailure)
               .NotEmpty().WithMessage("Polje ne moze biti prazno")
               .Length(1, 60).WithMessage("Duzina mora biti izmedju 1 i 60 karaktera")
               .Must(BeAValidName).WithMessage("Morate uneti Naziv artikla koji je sacinjen od slova.");
        }

        protected bool BeAValidName(string NazivTipa)
        {
            NazivTipa = NazivTipa.Replace(" ", "");
            NazivTipa = NazivTipa.Replace("-", "");
            return NazivTipa.All(Char.IsLetterOrDigit);
        }
    }
}
