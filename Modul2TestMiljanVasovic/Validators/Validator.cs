﻿using FluentValidation;
using Modul2TestMiljanVasovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.Validators
{
    public class Validator:AbstractValidator<ArtikalModel>
    {
        
        public Validator()
        {
            RuleFor(p => p.Id)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().WithMessage("Niste uneli obavezna polja");

            RuleFor(p => p.IdKategorije)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().WithMessage("Niste uneli obavezna polja");

            RuleFor(p => p.Cena)                
                .Cascade(CascadeMode.StopOnFirstFailure)
                .LessThan(0).WithMessage("cena mora biti pozitivna vrednost")
                .NotEmpty().WithMessage("Morate uneti cenu.");

            RuleFor(p => p.IdTipa)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().WithMessage("Niste uneli obavezna polja");

            RuleFor(p => p.Marka)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("Morate uneti marku.")
                .Length(1, 60).WithMessage("Duzina mora biti izmedju 1 i 60 karaktera");

            RuleFor(p => p.NazivArtikla)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("Polje ne moze biti prazno")
                .Length(1,60).WithMessage("Duzina mora biti izmedju 1 i 60 karaktera")
                .Must(BeAValidName).WithMessage("Morate uneti Naziv artikla koji je sacinjen od slova.");

            RuleFor(p => p.Opis)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("Morate uneti opis.");

            
        }
        protected bool BeAValidName(string NazivArtikla)
        {
            NazivArtikla = NazivArtikla.Replace(" ", "");
            NazivArtikla = NazivArtikla.Replace("-", "");
            return NazivArtikla.All(Char.IsLetterOrDigit);
        }
    }
}