﻿using FluentValidation.Results;
using Modul2TestMiljanVasovic.Models;
using Modul2TestMiljanVasovic.Models.Repository;
using Modul2TestMiljanVasovic.Models.ViewModels;
using Modul2TestMiljanVasovic.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Modul2TestMiljanVasovic.Controllers
{
    public class KategorijaOdeceController: Controller
    {
        
        private IrepositoryKategorijaOdece kategorijaRepo =new KategorijaRepo();

        BindingList<string> errors = new BindingList<string>();

        KategorijaOdeceModel KategorijaOdeceModel = new KategorijaOdeceModel();

        // GET: TipMotora

        public ActionResult Index()
        {
            var kategorije = kategorijaRepo.GetAll();

            return View(kategorije);
        }
        //Get
        public ActionResult Details(int id)
        {
            var kategorijeOdece = kategorijaRepo.GetById(id);

            return View(kategorijeOdece);
        }
        //get
        public ActionResult Create()
        {
            return View();
        }

       
        [HttpPost]
        public ActionResult Create(ArtikalKategorijaViewModel artikalKategorijaViewModel)
        {
            
            KategorijaOdeceModel KategorijaOdeceModel = new KategorijaOdeceModel() { KategorijaId=artikalKategorijaViewModel.KategorijaOdeceModel.KategorijaId,NazivKategorije=artikalKategorijaViewModel.KategorijaOdeceModel.NazivKategorije};
            KategorijaRepo kategorijaRepo = new KategorijaRepo();
           

            //Validacija
            Validator1 validator1 = new Validator1();
            FluentValidation.Results.ValidationResult results = validator1.Validate(KategorijaOdeceModel);

            if (results.IsValid == false)
            {
                foreach (ValidationFailure failure in results.Errors)
                {
                    errors.Add($"{failure.PropertyName}:{failure.ErrorMessage}");

                }
            }
            if (kategorijaRepo.Create(KategorijaOdeceModel) == false)
            {

                return View(artikalKategorijaViewModel);
            }

            return RedirectToAction("Index");   // nakon uspesnog dodavanja daj listing svih proizvoda
           
            
        }




        public ActionResult Brisi(int id)
        {
            kategorijaRepo.Delete(id);
            return RedirectToAction("Index");
        }

        // [HttpGet]
        public ActionResult Izmeni(int id)
        {

            KategorijaRepo kategorijaRepo = new KategorijaRepo();
            var data = kategorijaRepo.GetById(id);

            KategorijaOdeceModel kategorijaOdeceModel1 = new KategorijaOdeceModel() {KategorijaId=data.KategorijaId,NazivKategorije=data.NazivKategorije };
            ArtikalKategorijaViewModel artikalKategorijaViewModel = new ArtikalKategorijaViewModel() {  KategorijaOdeceModel=kategorijaOdeceModel1 ,SelektovaniKategorejeOdeceId=data.KategorijaId };

            return View(artikalKategorijaViewModel);
        }
        [HttpPost]
        public ActionResult Izmeni(ArtikalKategorijaViewModel model)
        {
            KategorijaOdeceModel kategorijaOdeceModel = new KategorijaOdeceModel() {KategorijaId=model.KategorijaOdeceModel.KategorijaId,NazivKategorije=model.KategorijaOdeceModel.NazivKategorije};

            KategorijaRepo kategorijaRepo = new KategorijaRepo();
            kategorijaRepo.Update(kategorijaOdeceModel);

            return RedirectToAction("Index");

        }
    }
}
