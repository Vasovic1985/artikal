﻿using FluentValidation;
using FluentValidation.Results;
using Modul2TestMiljanVasovic.Models;
using Modul2TestMiljanVasovic.Models.Repository;
using Modul2TestMiljanVasovic.Models.ViewModels;
using Modul2TestMiljanVasovic.Validators;
using System;
using PagedList.Mvc;
using PagedList;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Modul2TestMiljanVasovic.Controllers
{
    public class ArtikalController : Controller
    {

        // GET: Artikal
        private IrepositoryArtikal artikalRepo = new ArtikalRepo();

        BindingList<string> errors = new BindingList<string>();

        ArtikalKategorijaViewModel artikalKategorijaViewModel = new ArtikalKategorijaViewModel();
        
      

        // GET: Auto
        public ActionResult Index(string searchString,int? page)
        {

            int pageSize = 7;
            
            int pageNumber = (page ?? 1);
           
            
            
                if (!String.IsNullOrEmpty(searchString))
                {
                    var artikli = artikalRepo.GetAll();
                    artikli = artikli.Where(s => s.NazivArtikla.Contains(searchString) ||
                    s.Opis.Contains(searchString) ||
                    s.Marka.Contains(searchString));

               
                    return View(artikli.ToPagedList(pageNumber, pageSize));
                }
            
           

                
                
            return View(artikalRepo.GetAll().ToPagedList(pageNumber, pageSize));


            
        }

        public ActionResult Detalji(int id)
        {
            var artikal = artikalRepo.GetById(id);

            return View(artikal);
        }
       

        //Get
        public ActionResult Create()
        {


            return View();
        }
        [HttpPost]
        public ActionResult Create(ArtikalKategorijaViewModel artikalKategorijaViewModel)
        {

            
                ArtikalModel artikalModel1 = new ArtikalModel() {
                    NazivArtikla = artikalKategorijaViewModel.ArtikalModel.NazivArtikla,
                    Cena = artikalKategorijaViewModel.ArtikalModel.Cena,
                    Marka = artikalKategorijaViewModel.ArtikalModel.Marka,
                    Opis = artikalKategorijaViewModel.ArtikalModel.Opis,
                    IdKategorije = artikalKategorijaViewModel.SelektovaniKategorejeOdeceId,
                    IdTipa = artikalKategorijaViewModel.SelektovaniTipOdeceId };

                ArtikalRepo artikalRepo = new ArtikalRepo();




            


            //}
            //Validacija
            Validator validator = new Validator();
            FluentValidation.Results.ValidationResult results = validator.Validate(artikalModel1);

            if (results.IsValid==false)
            {
                foreach (ValidationFailure failure in results.Errors)
                {
                    errors.Add($"{failure.PropertyName}:{failure.ErrorMessage}");
                    
                }
            }
            //       
            if (artikalRepo.Create(artikalModel1)==false)
            {

                return View(artikalKategorijaViewModel);
            }

            return RedirectToAction("Index");



        }








        public ActionResult Brisi(int id)
        {
            artikalRepo.Delete(id);

            return RedirectToAction("Index");
        }

        // [HttpGet]

        public ActionResult Izmeni(int id)
        {
            ArtikalRepo artikalRepo = new ArtikalRepo();
            ArtikalModel data = artikalRepo.GetById(id);

            ArtikalModel artikalModel1 = new ArtikalModel() { Id = data.Id,NazivArtikla=data.NazivArtikla,Cena=data.Cena,Marka=data.Marka,Opis=data.Opis,IdKategorije=data.IdKategorije,IdTipa=data.IdTipa };



            ArtikalKategorijaViewModel artikalKategorijaViewModel = new ArtikalKategorijaViewModel() { SelektovaniKategorejeOdeceId=data.IdKategorije, ArtikalModel=artikalModel1 };


            return View(artikalKategorijaViewModel);
        }
        [HttpPost]
        public ActionResult Izmeni(ArtikalKategorijaViewModel model)
        {

            ArtikalModel artikalModel = new ArtikalModel() { Id = model.ArtikalModel.Id,NazivArtikla=model.ArtikalModel.NazivArtikla,Cena=model.ArtikalModel.Cena,Marka=model.ArtikalModel.Marka,Opis=model.ArtikalModel.Opis,IdKategorije=model.SelektovaniKategorejeOdeceId,IdTipa=model.SelektovaniTipOdeceId};

            ArtikalRepo artikalRepo = new ArtikalRepo();
            artikalRepo.Update(artikalModel);

            return RedirectToAction("Index");
        }
    }
}