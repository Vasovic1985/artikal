﻿using FluentValidation.Results;
using Modul2TestMiljanVasovic.Models;
using Modul2TestMiljanVasovic.Models.Repository;
using Modul2TestMiljanVasovic.Models.ViewModels;
using Modul2TestMiljanVasovic.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Modul2TestMiljanVasovic.Controllers
{
    public class TipOdeceController : Controller
    {
        // GET: TipOdece

        private ITipOdece tipRepo = new TipRepo();
         BindingList<string> errors = new BindingList<string>();


        // GET: TipMotora

        public ActionResult Index()
        {
            var tipoviOdece = tipRepo.GetAll();

            return View(tipoviOdece);
        }
        //Get
        public ActionResult Details(int id)
        {
            var tip1 = tipRepo.GetById(id);

            return View(tip1);
        }
        //get
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductCategory/Create
        [HttpPost]
        public ActionResult Create(ArtikalKategorijaViewModel model)
        {

            TipOdeceModel tipOdece1 = new TipOdeceModel() {TipId=model.tipOdeceModel.TipId,NazivTipa=model.tipOdeceModel.NazivTipa };
            TipRepo tipRepo = new TipRepo();

            //Validacija
            Validator2 validator2 = new Validator2();
            FluentValidation.Results.ValidationResult results = validator2.Validate(tipOdece1);

            if (results.IsValid == false)
            {
                foreach (ValidationFailure failure in results.Errors)
                {
                    errors.Add($"{failure.PropertyName}:{failure.ErrorMessage}");

                }
            }
            if (tipRepo.Create(tipOdece1) == false)
            {

                return View(model);
            }
            return RedirectToAction("Index");
        }




        public ActionResult Brisi(int id)
        {
            tipRepo.Delete(id);
            return RedirectToAction("Index");
        }

        // [HttpGet]
        public ActionResult Izmeni(int id)
        {

            TipRepo tipRepo = new TipRepo();
            var data = tipRepo.GetById(id);

            TipOdeceModel tipOdeceModel1 = new TipOdeceModel() {TipId=data.TipId,NazivTipa=data.NazivTipa };
            ArtikalKategorijaViewModel artikalKategorijaViewModel = new ArtikalKategorijaViewModel() { tipOdeceModel=tipOdeceModel1, SelektovaniTipOdeceId=data.TipId };

            return View(artikalKategorijaViewModel);
        }
        [HttpPost]
        public ActionResult Izmeni(ArtikalKategorijaViewModel artikalKategorijaViewModel)
        {
            TipOdeceModel tipOdeceModel = new TipOdeceModel() { TipId=artikalKategorijaViewModel.tipOdeceModel.TipId,NazivTipa=artikalKategorijaViewModel.tipOdeceModel.NazivTipa };

            TipRepo tipRepo = new TipRepo();
            tipRepo.Update(tipOdeceModel);

            return RedirectToAction("Index");

        }
    }




}
