﻿using Modul2TestMiljanVasovic.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.fonts.Helper
{
    public class List1
    {
        public static List<TipOdeceModel> GetAll()
        {
            string constr = ConfigurationManager.ConnectionStrings["ArtikalBazaPodataka"].ToString();

            SqlConnection con = new SqlConnection(constr);

            List<TipOdeceModel> listaTipova = new List<TipOdeceModel>();



            try
            {
                string query = "SELECT * FROM TipOdece";


                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "TipOdece"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["TipOdece"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                               // zatvori konekciju
                }
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int tipId = int.Parse(dataRow["TipId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string nazivTipa = dataRow["NazivTipa"].ToString();


                    listaTipova.Add(new TipOdeceModel() { TipId=tipId,NazivTipa=nazivTipa });  // ProductCategory = productCategoryRepo.GetById(productCategoryId) });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja kategorija proizvoda. {ex.StackTrace}");
                throw ex;
            }

            return listaTipova;

        }
    }
}
