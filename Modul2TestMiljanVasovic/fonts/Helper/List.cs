﻿using Modul2TestMiljanVasovic.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.fonts.Helper
{
    public class List
    {
       
            public static List<KategorijaOdeceModel> GetAll()
            {
                string constr = ConfigurationManager.ConnectionStrings["ArtikalBazaPodataka"].ToString();

                SqlConnection con = new SqlConnection(constr);

                List<KategorijaOdeceModel> listaKategroija = new List<KategorijaOdeceModel>();



                try
                {
                    string query = "SELECT * FROM KategorijaOdece";


                    DataTable dt = new DataTable(); // objekti u 
                    DataSet ds = new DataSet();     // koje smestam podatke

                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;

                        SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                        dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                        // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                        dadapter.Fill(ds, "KategorijaOdece"); // 'ProductCategory' je naziv tabele u dataset-u
                        dt = ds.Tables["KategorijaOdece"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                        con.Close();                               // zatvori konekciju
                    }
                    foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                    {
                        int kategorijaId = int.Parse(dataRow["KategorijaId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                        string nazivKategorije = dataRow["NazivKategorije"].ToString();


                    listaKategroija.Add(new KategorijaOdeceModel() { KategorijaId=kategorijaId,NazivKategorije=nazivKategorije });  // ProductCategory = productCategoryRepo.GetById(productCategoryId) });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Dogodila se greska prilikom izlistavanja kategorija proizvoda. {ex.StackTrace}");
                    throw ex;
                }

                return listaKategroija;

            }
        }
    }
