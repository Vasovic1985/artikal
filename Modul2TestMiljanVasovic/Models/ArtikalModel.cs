﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.Models
{
    public class ArtikalModel
    {
      
        public int Id { get; set; }
        //[Required]
        //[StringLength(40, MinimumLength = 1)]
        public string NazivArtikla { get; set; }
        //[Required]
        //[Range(0,float.MaxValue)]
        public float Cena { get; set; }
        //[Required]
        //[StringLength(40, MinimumLength = 1)]
        public string Marka { get; set; }

        public string Opis { get; set; }
       
        public int IdTipa { get; set; }
      
        public int IdKategorije { get; set; }
       
       
    }
}