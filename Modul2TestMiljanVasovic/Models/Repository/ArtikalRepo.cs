﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.Models.Repository
{
    public class ArtikalRepo: IrepositoryArtikal
    {
        public KategorijaRepo AutoRepo = new KategorijaRepo();
        private SqlConnection con;
        //"Data Source = (localdb)/ProjectsV13; Initial Catalog = KnjizaraBazaPodataka;"
        //User ID=sa;Password=SqlServer2016;

        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["ArtikalBazaPodataka"].ToString();
            //"Data Source = (localdb)/ProjectsV13; Initial Catalog =
            //Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False;
            con = new SqlConnection(constr);
        }


        public bool Create(ArtikalModel model)
        {
            try
            {
                string query = "INSERT INTO Artikal(IdKategorije,IdTipa,NazivArtikla,Cena,Marka,Opis,Obrisano)VALUES(@IdKategorije,@IdTipa,@NazivArtikla,@Cena,@Marka,@Opis,'false')";
                query += "SELECT SCOPE_IDENTITY()";
                Connection();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                   
                    cmd.Parameters.AddWithValue("@IdKategorije", model.IdKategorije);
                    cmd.Parameters.AddWithValue("@IdTipa", model.IdTipa);

                    cmd.Parameters.AddWithValue("@NazivArtikla", model.NazivArtikla);

                   
                    cmd.Parameters.AddWithValue("@Cena", model.Cena);
                    cmd.Parameters.AddWithValue("@Marka", model.Marka);
                    cmd.Parameters.AddWithValue("@Opis", model.Opis);

                    con.Open();
                    if (model.NazivArtikla == null || model.Marka == null || model.Opis == null||model.Cena<0)
                    {

                        return false;
                    }
                    var NoviId = cmd.ExecuteScalar();
                    con.Close();

                    if (NoviId != null)
                    {
                        return true;
                    }
                    cmd.Cancel();
                    return false;

                }
                
                
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa novog proizvoda. " + ex.StackTrace);  // ispis u output-prozorcicu
                throw ex;
            }
        }

        public IEnumerable<ArtikalModel> GetAll()
        {

            List<ArtikalModel> artikli = new List<ArtikalModel>();
            

            try
            {
                string query = "SELECT * FROM Artikal WHERE Artikal.Obrisano='false'"; //Za sada
                //string query = "SELECT * FROM Auto join TipMotora on Auto.IdMotora=TipMotora.IdMotora WHERE Auto.Obrisano='false'";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Artikal"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Artikal"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u

                    con.Close();                               // zatvori konekciju
                }
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int id = int.Parse(dataRow["Id"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string nazivArtikla = dataRow["NazivArtikla"].ToString();
                   
                    float cena = float.Parse(dataRow["Cena"].ToString());
                    string marka = dataRow["Marka"].ToString();
                    string opis = dataRow["Opis"].ToString();
                    int idTipa = int.Parse(dataRow["IdTipa"].ToString());
                    int idKategorije = int.Parse(dataRow["IdKategorije"].ToString());
                    

                    //var tipMotora1 = new AutoTipMotoraViewModel();


                    artikli.Add(new ArtikalModel() { Id = id,NazivArtikla=nazivArtikla,Cena=cena,Marka=marka,Opis=opis,IdTipa=idTipa,IdKategorije=idKategorije });  // ProductCategory = productCategoryRepo.GetById(productCategoryId) });
                                                                                                                                                                                         // motori.Add(new TipMotora() { NazivMotora=imeMotora,IdMotora=idMotora});
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja kategorija proizvoda. {ex.StackTrace}");
                throw ex;
            }

            return artikli;

        }






        public ArtikalModel GetById(int id)
        {

            ArtikalModel model = null;

            try
            {
                string query = "SELECT * FROM Artikal WHERE Id = @Id;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Id", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Artikal"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Artikal"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }


                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int ID = int.Parse(dataRow["Id"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string nazivArtikla = dataRow["NazivArtikla"].ToString();

                    float cena = float.Parse(dataRow["Cena"].ToString());
                    string marka = dataRow["Marka"].ToString();
                    string opis = dataRow["Opis"].ToString();
                    int idTipa = int.Parse(dataRow["IdTipa"].ToString());
                    int idKategorije = int.Parse(dataRow["IdKategorije"].ToString());

                    model = new ArtikalModel() { Id =ID,NazivArtikla=nazivArtikla,Cena=cena,Marka=marka,Opis=opis,IdTipa=idTipa,IdKategorije=idKategorije };
                }
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja kategorije proizvoda sa id-om: {model.Id}. {e.StackTrace}");
                throw e;
            }

            return model;

        }
        

        public void Delete(int id)
        {

            try
            {
                //string query = "DELETE FROM Auto WHERE Id = @Id;";
                string query = "update Artikal set Obrisano='true' WHERE Id = @Id";

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@Id", id);  // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja kategorije proizvoda. {e.StackTrace}");
                throw e;
            }


        }

        public void Update(ArtikalModel model)
        {

            try
            {
                string query = "UPDATE Artikal SET NazivArtikla = @NazivArtikla,Cena=@Cena,Marka=@Marka,Opis=@Opis,IdTipa=@IdTipa,IdKategorije=@IdKategorije WHERE Id = @Id";
                //string query = "UPDATE Knjiga SET Naslov = @Naslov, Cena = @Cena, ZanrId = @ZanrId WHERE Id = @Id";

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@Id", model.Id);  // stitimo od SQL Injection napada
                    cmd.Parameters.AddWithValue("@NazivArtikla", model.NazivArtikla);
                    cmd.Parameters.AddWithValue("@Cena", model.Cena);
                    cmd.Parameters.AddWithValue("@Marka", model.Marka);
                    cmd.Parameters.AddWithValue("@Opis", model.Opis);
                    cmd.Parameters.AddWithValue("@IdTipa", model.IdTipa);
                    cmd.Parameters.AddWithValue("@IdKategorije", model.IdKategorije);

                   


                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja kategorije proizvoda. {e.StackTrace}");
                throw e;
            }

        }


    }

}
