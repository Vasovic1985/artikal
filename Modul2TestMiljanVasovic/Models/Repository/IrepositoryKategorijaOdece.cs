﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul2TestMiljanVasovic.Models.Repository
{
    interface IrepositoryKategorijaOdece
    {
        IEnumerable<KategorijaOdeceModel> GetAll();
        KategorijaOdeceModel GetById(int id);
        bool Create(KategorijaOdeceModel model);
        void Delete(int id);
        void Update(KategorijaOdeceModel model);
    }
}
