﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul2TestMiljanVasovic.Models.Repository
{
    interface ITipOdece
    {
        IEnumerable<TipOdeceModel> GetAll();
        TipOdeceModel GetById(int id);
        bool Create(TipOdeceModel model);
        void Delete(int id);
        void Update(TipOdeceModel model);
    }
}
