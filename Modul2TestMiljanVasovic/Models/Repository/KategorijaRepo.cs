﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.Models.Repository
{
  
       

        public class KategorijaRepo : IrepositoryKategorijaOdece
        {
            // public AutoRepository tipRepo = new AutoRepository();
            private SqlConnection con;
            //"Data Source = (localdb)/ProjectsV13; Initial Catalog = KnjizaraBazaPodataka;"
            //User ID=sa;Password=SqlServer2016;

            private void Connection()
            {
                string constr = ConfigurationManager.ConnectionStrings["ArtikalBazaPodataka"].ToString();
                //"Data Source = (localdb)/ProjectsV13; Initial Catalog =
                //Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False;
                con = new SqlConnection(constr);
            }


            public bool Create(KategorijaOdeceModel model)
            {
                try
                {
                    string query = "INSERT INTO KategorijaOdece(NazivKategorije,Obrisano)VALUES(@NazivKategorije,'false')";
                    query += "SELECT SCOPE_IDENTITY()";
                    Connection();
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;
                        
                        cmd.Parameters.AddWithValue("@NazivKategorije", model.NazivKategorije);


                        con.Open();
                    if (model.NazivKategorije==null)
                    {
                        return false;
                    }
                        var NoviId = cmd.ExecuteScalar();
                        con.Close();

                        if (NoviId != null)
                        {
                            return true;
                        }
                        return false;

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Dogodila se greska pilikom upisa novog proizvoda. " + ex.StackTrace);  // ispis u output-prozorcicu
                    throw ex;
                }
            }

            public IEnumerable<KategorijaOdeceModel> GetAll()
            {

                List<KategorijaOdeceModel> ListaKategorija = new List<KategorijaOdeceModel>();

                try
                {
                    string query = "SELECT * FROM KategorijaOdece WHERE Obrisano='false'";
                    Connection();   // inicijaizuj novu konekciju

                    DataTable dt = new DataTable(); // objekti u 
                    DataSet ds = new DataSet();     // koje smestam podatke

                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;

                        SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                        dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                        // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                        dadapter.Fill(ds, "KategorijaOdece"); // 'ProductCategory' je naziv tabele u dataset-u
                        dt = ds.Tables["KategorijaOdece"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                        con.Close();                               // zatvori konekciju
                    }
                    foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                    {
                        int kategorijaId = int.Parse(dataRow["KategorijaId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                        string nazivKategorije = dataRow["NazivKategorije"].ToString();


                        ListaKategorija.Add(new KategorijaOdeceModel() { NazivKategorije=nazivKategorije,KategorijaId=kategorijaId });  // ProductCategory = productCategoryRepo.GetById(productCategoryId) });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Dogodila se greska prilikom izlistavanja kategorija proizvoda. {ex.StackTrace}");
                    throw ex;
                }

                return ListaKategorija;

            }






            public KategorijaOdeceModel GetById(int id)
            {

                KategorijaOdeceModel model = null;

                try
                {
                    string query = "SELECT * FROM KategorijaOdece WHERE KategorijaId = @KategorijaId";
                    Connection();   // inicijaizuj novu konekciju

                    DataTable dt = new DataTable(); // objekti u 
                    DataSet ds = new DataSet();     // koje smestam podatke

                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;
                        cmd.Parameters.AddWithValue("@KategorijaId", id);//izmenio

                        SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                        dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                        // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                        dadapter.Fill(ds, "KategorijaOdece"); // 'ProductCategory' je naziv tabele u dataset-u
                        dt = ds.Tables["KategorijaOdece"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                        con.Close();                  // zatvori konekciju
                    }


                    foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                    {
                        int kategorijaId = int.Parse(dataRow["KategorijaId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                        string nazivKategorije = dataRow["NazivKategorije"].ToString();

                        model = new KategorijaOdeceModel() { KategorijaId=kategorijaId,NazivKategorije=nazivKategorije };
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Dogodila se greska prilikom preuzimanja kategorije proizvoda sa id-om:  {e.StackTrace}");
                    throw e;
                }

                return model;

            }

            public void Delete(int id)
            {

                try
                {
                    //string query = "DELETE FROM TipMotora WHERE IdMotora = @IdMotora";
                    string query = "update KategorijaOdece set Obrisano='true' WHERE KategorijaId = @KategorijaId";

                    Connection();   // inicijaizuj novu konekciju

                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                        cmd.Parameters.AddWithValue("@KategorijaId", id);  // stitimo od SQL Injection napada

                        con.Open();                                         // otvori konekciju
                        cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                        con.Close();                                        // zatvori konekciju
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Dogodila se greska prilikom brisanja kategorije proizvoda. {e.StackTrace}");
                    throw e;
                }


            }

            public void Update(KategorijaOdeceModel model)
            {

                try
                {
                    string query = "UPDATE KategorijaOdece SET NazivKategorije = @NazivKategorije,Obrisano='false' WHERE KategorijaId = @KategorijaId";
                   
                    Connection();   // inicijaizuj novu konekciju

                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                        cmd.Parameters.AddWithValue("@KategorijaId", model.KategorijaId);  // stitimo od SQL Injection napada
                        cmd.Parameters.AddWithValue("@NazivKategorije", model.NazivKategorije);


                        con.Open();                                         // otvori konekciju
                        cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                        con.Close();                                        // zatvori konekciju
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Dogodila se greska prilikom izmene kategorije proizvoda. {e.StackTrace}");
                    throw e;
                }

            }
        }
    }
