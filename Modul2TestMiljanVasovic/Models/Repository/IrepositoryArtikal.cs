﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul2TestMiljanVasovic.Models.Repository
{
    interface IrepositoryArtikal
    {
        IEnumerable<ArtikalModel> GetAll();
        ArtikalModel GetById(int id);        
        bool Create(ArtikalModel model);
        void Delete(int id);
        void Update(ArtikalModel model);
    }
}
