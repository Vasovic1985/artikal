﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.Models.Repository
{
    public class TipRepo:ITipOdece
    {
        // public AutoRepository tipRepo = new AutoRepository();
        private SqlConnection con;
        //"Data Source = (localdb)/ProjectsV13; Initial Catalog = KnjizaraBazaPodataka;"
        //User ID=sa;Password=SqlServer2016;

        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["ArtikalBazaPodataka"].ToString();
            //"Data Source = (localdb)/ProjectsV13; Initial Catalog =
            //Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False;
            con = new SqlConnection(constr);
        }


        public bool Create(TipOdeceModel model)
        {
            try
            {
                string query = "INSERT INTO TipOdece(NazivTipa,Obrisano)VALUES(@NazivTipa,'false')";
                query += "SELECT SCOPE_IDENTITY()";
                Connection();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    //cmd.Parameters.AddWithValue("@Id", model.Id);
                    // cmd.Parameters.AddWithValue("@IdMotora", model.IdMotora);
                    cmd.Parameters.AddWithValue("@NazivTipa", model.NazivTipa);


                    con.Open();
                    if (model.NazivTipa==null)
                    {
                        return false;
                    }
                    var NoviId = cmd.ExecuteScalar();
                    con.Close();

                    if (NoviId != null)
                    {
                        return true;
                    }
                    return false;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa novog proizvoda. " + ex.StackTrace);  // ispis u output-prozorcicu
                throw ex;
            }
        }

        public IEnumerable<TipOdeceModel> GetAll()
        {

            List<TipOdeceModel> listaTipova = new List<TipOdeceModel>();

            try
            {
                string query = "SELECT * FROM TipOdece WHERE Obrisano='false'";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "TipOdece"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["TipOdece"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                               // zatvori konekciju
                }
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int tipId = int.Parse(dataRow["TipId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string nazivTipa = dataRow["NazivTipa"].ToString();


                    listaTipova.Add(new TipOdeceModel() { TipId=tipId,NazivTipa=nazivTipa });  // ProductCategory = productCategoryRepo.GetById(productCategoryId) });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja kategorija proizvoda. {ex.StackTrace}");
                throw ex;
            }

            return listaTipova;

        }






        public TipOdeceModel GetById(int id)
        {

            TipOdeceModel model = null;

            try
            {
                string query = "SELECT * FROM TipOdece WHERE TipId = @TipId";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@TipId", id);//izmenio

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "TipOdece"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["TipOdece"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }


                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int tipId = int.Parse(dataRow["TipId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string nazivTipa = dataRow["NazivTipa"].ToString();

                    model = new TipOdeceModel() { TipId=tipId,NazivTipa=nazivTipa };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja kategorije proizvoda sa id-om:  {e.StackTrace}");
                throw e;
            }

            return model;

        }

        public void Delete(int id)
        {

            try
            {
                //string query = "DELETE FROM TipMotora WHERE IdMotora = @IdMotora";
                string query = "update TipOdece set Obrisano='true' WHERE TipId = @TipId";

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@TipId", id);  // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja kategorije proizvoda. {e.StackTrace}");
                throw e;
            }


        }

        public void Update(TipOdeceModel model)
        {

            try
            {
                string query = "UPDATE TipOdece SET NazivTipa = @NazivTipa,Obrisano='false' WHERE TipId = @TipId";
                //IdMotora=@IdMotora,
                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@TipId", model.TipId);  // stitimo od SQL Injection napada
                    cmd.Parameters.AddWithValue("@NazivTipa", model.NazivTipa);


                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Dogodila se greska prilikom izmene kategorije proizvoda. {e.StackTrace}");
                throw e;
            }

        }
    }
}
