﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.Models
{
    public class KategorijaOdeceModel
    {
        public int KategorijaId { get; set; }
        public string NazivKategorije { get; set; }
    }
}