﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.Models
{
    public class TipOdeceModel
    {
        public int TipId { get; set; }
        public string NazivTipa { get;set; }
    }
}