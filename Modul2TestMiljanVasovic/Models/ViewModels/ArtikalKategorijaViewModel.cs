﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modul2TestMiljanVasovic.Models.ViewModels
{
    public class ArtikalKategorijaViewModel
    {
        public ArtikalModel ArtikalModel { get; set; }

        public KategorijaOdeceModel KategorijaOdeceModel { get; set; }
        public int SelektovaniKategorejeOdeceId { get; set; }

        public TipOdeceModel tipOdeceModel { get; set; }
        public int SelektovaniTipOdeceId { get; set; }
    }
}